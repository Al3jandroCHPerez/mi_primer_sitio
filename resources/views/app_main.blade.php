<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <title>@yield('title','Proyecto_0.0.1')</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <style>

    .head {
      top: 0em;
      right: 0em;
      position: absolute;
      display: table-cell;
      vertical-align: top;
      height: 10%;
  	width:100%; /* Establecemos que el header abarque el 100% del documento */
  	overflow:hidden; /* Eliminamos errores de float */
  	background:#252932;
  	margin:0px;
  }

  .wrapper {
  	width:100%; /* Establecemos que el ancho sera del 90% */
  	max-width:1000px; /* Aqui le decimos que el ancho máximo sera de 1000px */
  	margin:auto; /* Centramos los elementos */
  	overflow:hidden; /* Eliminamos errores de float */
  }

  header .logo {
  	color:#f2f2f2;
  	font-size:30px;
  	line-height:60px;
  	float:left;
  }

  header nav {
  	float:right;
  	line-height:65px;
  }

  header nav a {
  	display:inline-block;
  	color:#fff;
  	text-decoration:none;
  	padding:10px 20px;
  	line-height:normal;
  	font-size:20px;
  	font-weight:bold;
  	-webkit-transition:all 500ms ease;
  	-o-transition:all 500ms ease;
  	transition:all 500ms ease;
  }

  header nav a:hover {
  	background:#f56f3a;
  	border-radius:50px;
  }

  .body {
    background-color: #eaeaea;
  }
    </style>
</head>

<body class="body">
<header class="head">
		<div class="wrapper">
			<div class="logo">
        <a href="/">Proyecto_0.0.1</a>
      </div>
     	<nav>
        <a href="/about">Acerca de</a>
        <a href="/sing_in">Iniciar sesión</a>
        <a href="/register">Registrarme</a>
			</nav>
		</div>
	</header>
<div class="">
&nbsp;
<div>
  &nbsp;
  @yield('content')
</div>
</div>
</body>
</html>
